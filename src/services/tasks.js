let tasks = [];

function create(task) {
  tasks = [...tasks, task];
  console.log(tasks);
}

function read() {
  return tasks;
}

export default {
  create,
  read,
};
